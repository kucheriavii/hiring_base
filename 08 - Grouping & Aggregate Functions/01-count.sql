-- SELECT category, COUNT(id)
-- FROM tables
-- GROUP BY category;

-- SELECT COUNT(*)
-- FROM bookings;

-- SELECT COUNT(amount_tipped)
-- FROM bookings;

SELECT COUNT(DISTINCT booking_date)
FROM bookings;

