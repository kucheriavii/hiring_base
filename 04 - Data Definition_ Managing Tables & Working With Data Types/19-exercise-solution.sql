-- CREATE DATABASE online_shop;

--  Task 2 + 3: Create and configure table

-- CREATE TABLE products (
--     name VARCHAR(200), 
--     price NUMERIC(10, 2),
--     description TEXT,
--     amount_in_stock SMALLINT,
--     image_path TEXT
-- );

-- Task 4: Inserting dummy data

-- INSERT INTO products (price, name, description, amount_in_stock, image_path) 
-- VALUES (12.99, 'A Book', 'This is a book', 39, 'uploads/images/products/a-book.jpg');

-- Task 5: Add constrains

-- ALTER TABLE products
-- MODIFY COLUMN name VARCHAR(200) NOT NULL,
-- MODIFY COLUMN price NUMERIC(10,2) NOT NULL CHECK (price > 0),
-- MODIFY COLUMN description TEXT NOT NULL,
-- MODIFY COLUMN amount_in_stock SMALLINT CHECK (amount_in_stock >= 0);

-- Task 6: add id column

ALTER TABLE products
ADD COLUMN id INT PRIMARY KEY AUTO_INCREMENT;