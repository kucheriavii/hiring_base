DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS employers;
DROP TABLE IF EXISTS conversations;

CREATE TABLE IF NOT EXISTS users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    full_name VARCHAR(300) NOT NULL,
    yearly_salary INT CHECK (yearly_salary>0),
    current_status ENUM('employed','self-employed','unemployed')
);

CREATE TABLE employers (
    id INT PRIMARY KEY AUTO_INCREMENT
    , company_name VARCHAR(300) NOT NULL
    , company_address VARCHAR(300) NOT NULL
    -- , yearly_revenue FLOAT(18,2) --Approximation
    , yearly_revenue FLOAT CHECK (yearly_revenue>0) 
    , is_hiring BOOLEAN DEFAULT FALSE
);

CREATE TABLE conversations(
    id INT PRIMARY KEY AUTO_INCREMENT,    
    user_id INT,
    employer_id INT,
    message TEXT NOT NULL
);

