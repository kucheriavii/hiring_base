-- SELECT CONCAT(EXTRACT(DAY FROM last_checkin), 
--     ".", 
--     EXTRACT(MONTH FROM last_checkin)) 
-- FROM memberships;

-- SELECT EXTRACT(DAY FROM last_checkin)
-- FROM memberships;

-- SELECT WEEKDAY(last_checkin), last_checkin
-- FROM memberships;

-- SELECT CONVERT(last_checkin, DATE), CONVERT(last_checkin, TIME)
-- FROM memberships;

SELECT DATE(last_checkin) as last_checkin_date, TIME(last_checkin) as last_checkin_time
FROM memberships;