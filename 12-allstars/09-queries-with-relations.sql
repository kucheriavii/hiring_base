-- SELECT e.id, 
--     e.name, 
--     e.date_planned, 
--     loc.title, 
--     loc.street, 
--     loc.house_number, 
--     loc.city_name,
--     u.first_name,
--     u.last_name 
-- FROM events as e
-- INNER JOIN locations as loc ON e.location_id = loc.id
-- INNER JOIN events_users as eu ON eu.event_id = e.id
-- INNER JOIN users AS u ON eu.user_id = u.id;

-- SELECT * 
-- FROM locations as loc
-- LEFT JOIN events AS e ON e.location_id = loc.id;

SELECT * FROM cities as c
LEFT JOIN locations as loc ON loc.city_name = c.name
LEFT JOIN events as e ON e.location_id = loc.id
WHERE e.date_planned > '2020-01-01';