-- INSERT INTO users (first_name)
-- VALUES ('Manuel'), ('Max'), ('Julie');

-- INSERT INTO users_friends (user_id, friend_id)
-- VALUES (1,2), (1,3);

SELECT u.first_name, u2.first_name 
FROM users_friends AS uf
LEFT JOIN users as u ON uf.user_id = u.id 
LEFT JOIN users as u2 ON uf.friend_id = u2.id