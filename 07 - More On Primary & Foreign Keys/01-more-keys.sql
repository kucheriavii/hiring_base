 CREATE TABLE IF NOT EXISTS projects (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(300) NOT NULL,
    deadline DATE
 );

 CREATE TABLE IF NOT EXISTS company_buildings(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(300) NOT NULL    
 );

 CREATE TABLE IF NOT EXISTS teams (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(300) NOT NULL,
    building_id INT REFERENCES company_buildings (id) ON DELETE SET NULL 
 );

 CREATE TABLE IF NOT EXISTS employees (
    id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(300) NOT NULL,
    last_name VARCHAR(300) NOT NULL,
    birthdate DATE NOT NULL,
    -- email VARCHAR(200) REFERENCES intranet_accounts ON DELETE 
    email VARCHAR(200) UNIQUE NOT NULL,
    team_id INT DEFAULT 1 REFERENCES teams(id) ON DELETE SET NULL
 );

  CREATE TABLE IF NOT EXISTS intranet_accounts(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(200) REFERENCES employees (email) ON DELETE CASCADE,
    password VARCHAR(200) NOT NULL
 );

-- intermidiate table => n:n
 CREATE TABLE IF NOT EXISTS projects_employees (    
    employee_id INT,
    project_id INT REFERENCES projects ON DELETE CASCADE,
    PRIMARY KEY (employee_id, project_id),
    -- PRIMARY KEY (id) ...
    FOREIGN KEY (employee_id) REFERENCES employees(id) ON DELETE CASCADE
    -- FOREIGN KEY (employee_id, project_id) REFERENCES employees ON DELETE ... 
 );
 