SELECT e1.first_name, e1.last_name, e1.supervisor_id, e2.first_name 
FROM employees as e1
LEFT JOIN employees AS e2 on e1.supervisor_id = e2.id; 