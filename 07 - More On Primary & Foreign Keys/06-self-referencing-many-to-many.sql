DROP TABLE IF EXISTS users, users_friends;

CREATE TABLE IF NOT EXISTS users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(300) NOT NULL
);

CREATE TABLE users_friends (
    user_id INT REFERENCES user ON DELETE CASCADE,
    friend_id INT REFERENCES user ON DELETE CASCADE,
    CHECK (user_id < friend_id),
    PRIMARY KEY (user_id, friend_id)
);

