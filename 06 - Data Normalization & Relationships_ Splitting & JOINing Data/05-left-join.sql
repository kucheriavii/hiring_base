-- SELECT *
-- FROM users as u
-- LEFT JOIN addresses AS a ON a.id = u.address_id;

SELECT *
FROM addresses as a
LEFT JOIN users AS u ON u.address_id = a.id
LEFT JOIN cities AS c on c.id = a.city_id;
