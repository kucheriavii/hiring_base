-- SELECT e.id as employee_id, e.first_name, e.last_name, p.title FROM employees AS e
-- LEFT JOIN projects_employees AS pe ON pe.employee_id = e.id
-- LEFT JOIN projects AS p ON pe.project_id = p.id;
-- INNER JOIN projects_employees AS pe ON pe.employee_id = e.id
-- INNER JOIN projects AS p ON pe.project_id = p.id;


-- SELECT * FROM teams;

SELECT e.id AS employee_id, e.first_name, e.last_name, t.name, cb.name
FROM employees as e
INNER JOIN teams as t ON e.team_id = t.id
INNER JOIN company_buildings AS cb ON t.building_id = cb.id
WHERE cb.id = 3;