SELECT name as city_name, u.first_name, u.last_name, a.street, a.house_number
FROM cities AS c
LEFT JOIN addresses AS a ON a.city_id = c.id
LEFT JOIN users AS u on u.address_id = a.id;