INSERT INTO cities (name) 
VALUES ('Berlin'), ('New York'), ('London');

INSERT INTO addresses (street, house_number, city_id)
VALUES ('Teststreet', '8A', 3),
('Somestreet', '10', 1),
('Somestreet', '1', 3),
('Mystreet', '101', 2);

INSERT INTO users (first_name, last_name, email, address_id)
VALUES ('Tommy', 'Pizdjuk', 't.pizdjuk@pravda.com', 2),
       ('Manuel', 'Pizdjuk', 'm.pizdjuk@pravda.com', 4),
       ('Pamela', 'Anderson', 'p.under@porn.com', 3);

