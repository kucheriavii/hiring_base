-- SELECT *
-- FROM users as u
-- LEFT JOIN addresses AS a ON a.id = u.address_id;

SELECT *
FROM addresses as a
RIGHT JOIN users AS u ON u.address_id = a.id
RIGHT JOIN cities AS c on c.id = a.city_id;
