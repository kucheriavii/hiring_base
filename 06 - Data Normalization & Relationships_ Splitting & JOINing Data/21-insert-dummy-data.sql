INSERT INTO company_buildings (name)
VALUES
    ('Main Building'),
    ('Research Lab'),
    ('Darkroom');

INSERT INTO teams (name, building_id)
VALUES 
    ('Admin',1),
    ('Data Analyst',3),
    ('R&D',2);

INSERT INTO employees (first_name, last_name, birthdate, email, team_id)
VALUES
    ('Julie', 'Barnes', '1988-10-01', 'julie@test.com', 3),
    ('Max', 'Schwarz', '1989-06-10', 'max@test.com', 1),
    ('Manuel', 'BarnesLorenz', '1987-01-29', 'manu@test.com', 2);

INSERT INTO intranet_accounts (email, password)
VALUES
    ('max@test.com', 'abcae1'),
    ('manu@test.com', '1345678'),
    ('julie@test.com', 'qwerty123');